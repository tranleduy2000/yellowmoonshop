<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/8/2020
  Time: 11:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update cake</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Update cake</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <jsp:include page="include/message.jsp"/>
    <c:set var="cake" value="${requestScope.cake}" scope="request"/>
    <form method="post" action="UpdateCake" enctype="multipart/form-data">
        <input name="txtCakeId" value="${cake.cakeId}" hidden readonly>
        <div class="form-group">
            <label for="txtName">Name</label>
            <input name="txtName" id="txtName" maxlength="200" minlength="2" required
                   value="${cake.name}"
                   class="form-control">
            <div class="alert-danger">${requestScope.error.nameError}</div>
        </div>

        <div class="form-group">
            <label for="txtDescription">Description </label>
            <input type="text" name="txtDescription" id="txtDescription" class="form-control" maxlength="500"
                   value="${cake.description}">
            <div class="alert-danger">${requestScope.error.descriptionError}</div>
        </div>

        <img src="images/${cake.imagePath}" style="width: 100px; height: auto;" alt="${cake.name}">
        <div class="form-group">
            <label for="txtImageFile">Image</label>
            <input name="txtImageFile" id="txtImageFile" class="form-control-file" type="file" <%--required--%>>
            <div class="alert-danger">${requestScope.error.imagePathError}</div>
        </div>

        <div class="form-row align-items-end">
            <div class="form-group col-3">
                <label for="txtPrice">Price</label>
                <input type="number" name="txtPrice" id="txtPrice" required maxlength="15" class="form-control"
                       min="0"
                       value="${cake.price}">
                <div class="alert-danger">${requestScope.error.priceError}</div>
            </div>


            <div class="form-group col-3">
                <label for="txtQuantity">Quantity </label>
                <input type="number" name="txtQuantity" id="txtQuantity" required maxlength="15" class="form-control"
                       min="0" value="${cake.quantity}">
                <div class="alert-danger">${requestScope.error.quantityError}</div>
            </div>

            <div class="form-group col-4">
                <label for="cbxCategoryId">Category</label>
                <select class="form-control" name="cbxCategoryId" id="cbxCategoryId">
                    <c:forEach items="${requestScope.categories}" var="category">

                        <option value="${category.categoryId}"
                                <c:if test="${cake.categoryId.categoryId == category.categoryId}">selected</c:if>>
                                ${category.categoryId} ${category.name}
                        </option>
                    </c:forEach>
                </select>
                <div class="alert-danger">${requestScope.error.categoryIdError}</div>
            </div>

        </div>
        <div class="form-row">
            <div class="form-group col-3">
                <label for="txtCreateDate">Create date </label>
                <input type="date" id="txtCreateDate" name="txtCreateDate" required class="form-control"
                       value="<fmt:formatDate value="${cake.createDate}" pattern="yyyy-MM-dd"/>">
                <div class="alert-danger">${requestScope.error.createDateError}</div>
            </div>

            <div class="form-group col-3">
                <label for="txtExpirationDate">Expiration date </label>
                <input type="date" id="txtExpirationDate" name="txtExpirationDate" <%--required--%>
                       class="form-control"
                       value="<fmt:formatDate value="${cake.expirationDate}" pattern="yyyy-MM-dd"/>">
                <div class="alert-danger">${requestScope.error.expirationDateError}</div>
            </div>

            <div class="form-group col-3">
                <label for="checkBoxStatus">Status</label>
                <select id="checkBoxStatus" name="checkBoxStatus" class="form-control">
                    <option value="0" <c:if test="${!cake.status}">selected</c:if>>
                        Disable
                    </option>
                    <option value="1" <c:if test="${cake.status}">selected</c:if>>
                        Enable
                    </option>
                </select>
            </div>
        </div>
        <input type="submit" name="btnAction" value="Update" class="btn btn-primary">
    </form>
</div>
</body>
</html>
