<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/11/2020
  Time: 1:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Checkout</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Checkout</span>
</nav>
<div class="container my-2">
    <jsp:include page="include/message.jsp"/>
    <c:if test="${sessionScope.cart == null || sessionScope.cart.size() == 0}">
        <c:redirect url="ViewCake"/>
    </c:if>
    <c:if test="${sessionScope.cart != null && sessionScope.cart.size() > 0}">

        <h2>Your order</h2>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sessionScope.cart.cakes}" var="pair" varStatus="counter">
                <tr>
                    <td>${counter.index + 1}</td>
                    <td>${pair.key.name}</td>
                    <td>${pair.key.price}</td>
                    <td>${pair.value}</td>
                    <td>${pair.value * pair.key.price}</td>
                </tr>
            </c:forEach>
            <tr class="table-primary">
                <td colspan="4"><b>Total</b></td>
                <td>${sessionScope.cart.total}</td>
            </tr>
            </tbody>
        </table>

        <form action="MakePayment">
            <h2>Shipping information</h2>
            <div class="form-group row">
                <label for="txtName" class="col-2 col-form-label">Name (*)</label>
                <input type="text" name="txtName" id="txtName" class="form-control col-8"
                       value="${sessionScope.user.fullName}" required>
            </div>
            <div class="form-group row">
                <label for="txtPhoneNumber" class="col-2 col-form-label">Phone number (*)</label>
                <input type="text" name="txtPhoneNumber" id="txtPhoneNumber" class="form-control col-8"
                       value="${sessionScope.user.phoneNumber}" required>
            </div>
            <div class="form-group row">
                <label for="txtAddress" class="col-2 col-form-label">Shipping address (*)</label>
                <input type="text" name="txtAddress" id="txtAddress" class="form-control col-8" required>
            </div>
            <p class="font-italic">(*) required</p>
            <input type="submit" class="btn btn-primary" name="btnAction" value="Make Payment">
        </form>

    </c:if>
</div>
</body>
</html>
