<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/11/2020
  Time: 3:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order history</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-link bg-light">
    <span class="navbar-brand">Orders</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <jsp:include page="include/message.jsp"/>
    <a class="btn btn-outline-primary mx-2 my-2" href="orderDetails.jsp">Find order</a>
    <c:if test="${empty requestScope.orders}">
        <div class="alert alert-info">You did not make any purchase</div>
    </c:if>
    <c:if test="${not empty requestScope.orders}">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Order ID</th>
                <th>Time</th>
                <th></th>
            </tr>
            </thead>
            <c:forEach var="order" items="${requestScope.orders}" varStatus="counter">
                <tr>
                    <td>${counter.index + 1}</td>
                    <td>${order.orderId}</td>
                    <td>${order.time}</td>
                    <td><a class="btn btn-outline-info" href="ViewOrderDetails?txtOrderId=${order.orderId}">Details</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>
</div>
</body>
</html>
