<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/10/2020
  Time: 9:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cart</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Cart</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container my-2">
    <jsp:include page="include/message.jsp"/>
    <c:if test="${sessionScope.cart != null && sessionScope.cart.size() > 0}">
        <script>
            function confirmDeleteCakeFromCart(cakeId, quantity) {
                if (confirm('Are you sure?')) {
                    window.location.href = 'RemoveFromCart?txtCakeId=' + cakeId + '&txtQuantity=' + quantity;
                }
            }
        </script>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Amount</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${sessionScope.cart.cakes}" var="pair" varStatus="counter">
                <tr>
                    <td>${counter.index + 1}</td>
                    <td>${pair.key.name}</td>
                    <td><b>${pair.key.price}</b></td>
                    <td>
                        <a class="btn btn-outline-primary"
                           href="RemoveFromCart?txtCakeId=${pair.key.cakeId}&destination=cart.jsp">
                            <i class="fa fa-minus"></i></a>
                        <span>${pair.value}</span>
                        <a class="btn btn-outline-primary"
                           href="AddToCart?txtCakeId=${pair.key.cakeId}&destination=cart.jsp"><i
                                class="fa fa-plus"></i></a>
                    </td>
                    <td>$ ${pair.value * pair.key.price}</td>
                    <td>
                        <button class="btn btn-outline-danger"
                                onclick="confirmDeleteCakeFromCart('${pair.key.cakeId}', '${pair.value}')"><i
                                class="fa fa-remove"></i></button>
                    </td>
                </tr>
            </c:forEach>
            <tr class="table-primary">
                <td colspan="4">Total</td>
                <td colspan="2">
                    <span class="font-weight-bold">${sessionScope.cart.total}</span>
                </td>
            </tr>
            </tbody>
        </table>
        <button class="btn btn-primary" onclick="window.location.href='Checkout'">Purchase</button>
    </c:if>
    <c:if test="${sessionScope.cart == null || sessionScope.cart.size() == 0}">
        <div class="alert alert-info">Cart is empty</div>
    </c:if>
</div>
</body>
</html>
