<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 9/25/2020
  Time: 6:05 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update password</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Update password</span>
</nav>

<div class="container">
    <form method="post" action="UpdatePassword">
        <input type="hidden" name="txtUserId" value="${sessionScope.user.userId}">
        <div class="form-group">
            <label for="txtPassword">Old password</label>
            <input type="password" name="txtPassword" id="txtPassword" maxlength="20" minlength="2" required
                   class="form-control">
            <div class="alert-danger"> ${requestScope.error.passwordError}</div>
        </div>

        <div class="form-group">
            <label for="txtNewPassword">New password</label>
            <input type="password" id="txtNewPassword" name="txtNewPassword" maxlength="20" minlength="2" required
                   class="form-control">
            <div class="alert-danger">${requestScope.error.newPasswordError}</div>
        </div>

        <div class="form-group">
            <label for="txtRePassword">Retype password</label>
            <input type="password" name="txtRePassword" id="txtRePassword" maxlength="20" minlength="2" required
                   class="form-control">
            <div class="alert-danger">${requestScope.error.rePasswordError}</div>
        </div>

        <input type="submit" name="btnAction" value="Update password" class="btn btn-primary">
    </form>
</div>
</body>
</html>
