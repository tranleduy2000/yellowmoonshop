<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <title>Login Page</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <jsp:include page="metadata.jsp"/>

    <%--https://developers.google.com/identity/sign-in/web/sign-in--%>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id"
          content="277649074849-fne35gv09faukr8h7u5spqbu7hjcmh12.apps.googleusercontent.com">
    <script>
        function signOut() {
            const auth2 = gapi.auth2.getAuthInstance();
            auth2.signOut().then(function () {
                console.log('User signed out.');
            });
        }

        function onSignIn(googleUser) {
            const profile = googleUser.getBasicProfile();
            console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
            console.log('Name: ' + profile.getName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

            // https://developers.google.com/identity/sign-in/web/backend-auth#send-the-id-token-to-your-server
            const id_token = googleUser.getAuthResponse().id_token;
            signOut();

            //using jquery to post data dynamically
            const form = $(
                '<form action="LoginWithGoogle" method="post">' +
                '<input type="text" name="txtIdToken" value="' + id_token + '" />' +
                '</form>');
            $('body').append(form);
            form.submit();
        }
    </script>
</head>
<body>
<nav class="navbar navbar-link bg-light">
    <span class="navbar-brand">Login</span>
</nav>
<div class="container">

    <form method="post" action="Login">
        <div class="form-group">

            <label for="txtUserId"><i class="fa fa-envelope"></i> Email: </label><input id="txtUserId" name="txtUserId"
                                                                                        maxlength="50" required
                                                                                        class="form-control">
        </div>
        <div class="form-group">
            <label for="txtPassword"><i class="fa fa-key"></i> Password: </label><input id="txtPassword"
                                                                                        name="txtPassword"
                                                                                        maxlength="12"
                                                                                        type="password" required
                                                                                        class="form-control">
        </div>
        <c:if test="${not empty param.error}">
            <div class="alert alert-danger">${param.error}</div>
        </c:if>
        <input type="submit" name="btnAction" value="Login" class="btn btn-primary">
        <input type="reset" name="btnReset" class="btn btn-warning">
    </form>
    Not registered? <a href="createAccount.jsp">Create new account</a>
    <hr>

    Or continue with:
    <div class="g-signin2" data-onsuccess="onSignIn"></div>
</div>
</body>
</html>
