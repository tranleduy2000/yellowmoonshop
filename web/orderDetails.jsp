<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/11/2020
  Time: 2:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order details</title>
    <jsp:include page="metadata.jsp"/>
</head>

<body>
<nav class="navbar navbar-link bg-light">
    <span class="navbar-brand">Order details</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <jsp:include page="include/message.jsp"/>

    <form>
        <div class="form-group">
            <label for="txtOrderId">Order id</label>
            <input type="number" placeholder="Order id" required
                   value="${param.txtOrderId}"
                   class="form-control"
                   name="txtOrderId" id="txtOrderId">
        </div>
        <input type="submit" name="btnAction" value="View details" class="btn btn-primary">
    </form>

    <c:if test="${requestScope.order != null}">
        <c:set var="order" value="${requestScope.order}"/>
        <h1>Order #${order.orderId}</h1>
        <table class="table table-striped">
            <tr>
                <td>Time</td>
                <td>${order.time}</td>
            </tr>
            <tr>
                <td>Receiver name</td>
                <td>${order.receiverName}</td>
            </tr>
            <tr>
                <td>Phone number</td>
                <td>${order.receiverPhoneNumber}</td>
            </tr>
            <tr>
                <td>Shipping address</td>
                <td>${order.shippingAddress}</td>
            </tr>
            <tr>
                <td>Payment method</td>
                <td>${order.paymentMethodId.paymentName}</td>
            </tr>
            <c:choose>
                <c:when test="${order.paymentStatus == 1}">
                    <tr class="table-info">
                        <td>Payment status</td>
                        <td>Pending</td>
                    </tr>
                </c:when>
                <c:when test="${order.paymentStatus == 2}">
                    <tr class="table-success">
                        <td>Payment status</td>
                        <td>Success</td>
                    </tr>
                </c:when>
                <c:when test="${order.paymentStatus == 3}">
                    <tr class="table-danger">
                        <td>Payment status</td>
                        <td>Failed</td>
                    </tr>
                </c:when>
            </c:choose>
        </table>

        <h3>Cakes</h3>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${requestScope.orderDetails}" var="orderDetail" varStatus="counter">
                <tr>
                    <td>${counter.index + 1}</td>
                    <td>${orderDetail.cake.name}</td>
                    <td>${orderDetail.cake.price}</td>
                    <td>${orderDetail.quantity}</td>
                    <td>${orderDetail.quantity * orderDetail.cake.price}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>
</body>
</html>
