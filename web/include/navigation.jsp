<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/8/2020
  Time: 11:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="nav nav-pills">
    <li class="nav-item">
        <a class="nav-link" href="ViewCake"><i class="fa fa-home"></i>Home</a>
    </li>
    <c:if test="${sessionScope.user == null || sessionScope.user.roleId.roleId != 1}">
        <li class="nav-item">
            <a class="nav-link" href="cart.jsp"><i class="fa fa-shopping-cart"></i>Cart</a>
        </li>
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.user.roleId.roleId == 1}">
        <li class="nav-item">
            <a class="nav-link" href="CreateCake"><i class="fa fa-plus-circle"></i>Create cake</a>
        </li>
    </c:if>
    <c:if test="${sessionScope.user != null && sessionScope.user.roleId.roleId != 1}">
        <li class="nav-item">
            <a class="nav-link" href="TrackOrders"><i class="fa fa-history"></i>Track orders</a>
        </li>
    </c:if>
    <c:if test="${sessionScope.user == null}">
        <a class="nav-link" href="Login"><i class="fa fa-sign-in"></i>Login</a>
    </c:if>
    <c:if test="${sessionScope.user != null}">
        <li class="nav-item">
            <a class="nav-link" href="userDetails.jsp"><i class="fa fa-user"></i>Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="Logout"><i class="fa fa-sign-out"></i>Logout</a>
        </li>
    </c:if>
</ul>
