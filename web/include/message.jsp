<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/11/2020
  Time: 1:37 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${not empty param.message}">
    <div class="alert alert-success">${param.message}</div>
</c:if>
<c:if test="${not empty param.error}">
    <div class="alert alert-danger">${param.error}</div>
</c:if>