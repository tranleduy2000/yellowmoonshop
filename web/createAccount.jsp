<%--
    Document   : createAccount
    Created on : Sep 19, 2020, 12:46:53 AM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Create Account Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand h1">Create new account</span>
</nav>
<div class="container">
    <form method="post" action="CreateAccount">
        <div class="form-group">
            <label for="txtFullName">Full name (*)</label>
            <input name="txtFullName" id="txtFullName" maxlength="50" minlength="2" required
                   value="${param.txtFullName}"
                   class="form-control">
            <div class="alert-danger">${requestScope.error.fullNameError}</div>
        </div>

        <div class="form-group">
            <label for="txtUserId">Email (*) </label>
            <input name="txtUserId" id="txtUserId" maxlength="50" required type="email" class="form-control"
                   value="${param.txtUserId}">
            <div class="alert-danger">${requestScope.error.userIdError} </div>
        </div>

        <div class="form-group">
            <label for="txtPassword">Password (*)</label>
            <input type="password" name="txtPassword" id="txtPassword" maxlength="20" minlength="2" required
                   class="form-control">
            <div class="alert-danger"> ${requestScope.error.passwordError}</div>
        </div>
        <div class="form-group">
            <label for="txtRePassword">Retype password (*)</label>
            <input type="password" name="txtRePassword" id="txtRePassword" maxlength="20" minlength="2" required
                   class="form-control">
            <div class="alert-danger">${requestScope.error.rePasswordError}</div>
        </div>

        <div class="form-group">
            <label for="txtPhoneNumber">Phone number</label>
            <input type="text" name="txtPhoneNumber" id="txtPhoneNumber" maxlength="13" minlength="2"
                   value="${param.txtPhoneNumber}"
                   class="form-control">
            <div class="alert-danger">${requestScope.error.phoneNumberError}</div>
        </div>

        <input type="submit" name="btnAction" value="Create Account" class="btn btn-primary">
        <input type="reset" class="btn btn-warning">
    </form>
    <a href="login.jsp">Already has an account? Login</a>
</div>
</body>
</html>
