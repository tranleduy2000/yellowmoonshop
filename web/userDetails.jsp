<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    Document   : userDetails
    Created on : Sep 25, 2020, 5:40:17 PM
    Author     : duy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Profile Page</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand">Profile</span>
    <jsp:include page="include/navigation.jsp"/>
</nav>
<div class="container">
    <jsp:include page="include/message.jsp"/>
    <c:set var="user" value="${sessionScope.user}"/>

    <form method="post" action="UpdateAccount">
        <div class="form-group">
            <label for="txtFullName">Full name (*)</label>
            <input name="txtFullName" id="txtFullName" maxlength="50" minlength="2" required
                   value="${user.fullName}"
                   class="form-control">
            <div class="alert-danger">${requestScope.error.fullNameError}</div>
        </div>

        <div class="form-group">
            <label for="txtUserId">Email (*) </label>
            <input name="txtUserId" id="txtUserId" maxlength="50" required type="email" class="form-control"
                   value="${user.userId}" readonly>
            <div class="alert-danger">${requestScope.error.userIdError} </div>
        </div>

        <div class="form-group">
            <label for="txtPhoneNumber">Phone number</label>
            <input type="text" name="txtPhoneNumber" id="txtPhoneNumber" maxlength="13" minlength="2"
                   value="${user.phoneNumber}"
                   class="form-control">
            <div class="alert-danger">${requestScope.error.phoneNumberError}</div>
        </div>
        <input type="submit" name="btnAction" value="Update Account" class="btn btn-primary">
    </form>


    <a class="btn btn-outline-primary my-2" href="updatePassword.jsp">Update password</a>
</div>
</body>
</html>
