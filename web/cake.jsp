<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: duy
  Date: 10/8/2020
  Time: 11:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shopping</title>
    <jsp:include page="metadata.jsp"/>
</head>
<body>
<nav class="navbar navbar-link bg-light">
    <c:if test="${sessionScope.user != null}">
        <span class="navbar-brand">Shopping, Welcome ${sessionScope.user.fullName}</span>
    </c:if>
    <c:if test="${sessionScope.user == null}">
        <span class="navbar-brand">Shopping</span>
    </c:if>
    <jsp:include page="include/navigation.jsp"/>
</nav>

<%--Filter--%>
<div class="container my-2">
    <form action="ViewCake" method="post">
        <div class="form-row align-items-end">
            <div class="form-group col-3">
                <label for="txtCategory">Category</label>
                <select class="form-control" name="txtCategory" id="txtCategory">
                    <option value="">All</option>
                    <c:forEach items="${requestScope.categories}" var="category">
                        <option value="${category.categoryId}"
                                <c:if test="${param.txtCategory != null && param.txtCategory == category.categoryId}">
                                    selected
                                </c:if>>
                                ${category.categoryId} ${category.name}
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="col-4 form-group">
                <label for="txtCakeName">Name</label>
                <input class="form-control" name="txtCakeName" type="search" id="txtCakeName"
                       value="${param.txtCakeName}">
            </div>
            <div class="form-group col-2">
                <label for="txtMinPrice">Min price</label>
                <input class="form-control" type="number" name="txtMinPrice" value="${param.txtMinPrice}"
                       id="txtMinPrice"
                       placeholder="0">
            </div>
            <div class="form-group col-2">
                <label for="txtMaxPrice">Max price</label>
                <input class="form-control" type="number" name="txtMaxPrice" value="${param.txtMaxPrice}"
                       id="txtMaxPrice"
                       placeholder="+oo">
            </div>
            <div class="form-group col-1">
                <input class="btn btn-outline-success my-2 my-sm-0" type="submit" name="btnAction" value="Search"/>
            </div>
        </div>
    </form>
</div>
<div class="container">
    <jsp:include page="include/message.jsp"/>
    <c:if test="${not empty requestScope.cakes}">
        <div class="row">
            <c:forEach var="cake" items="${requestScope.cakes}" varStatus="counter">
                <div class="col-lg-4 col-sm-6">
                    <div class="card my-2">
                        <img class="card-img-top" src="images/${cake.imagePath}" alt="${cake.imagePath}"/>
                        <div class="card-body">
                            <h5 class="card-title">${cake.name} (${cake.quantity})</h5>
                            <span class="card-text font-italic">${cake.categoryId.name}</span>
                            <br>
                            <span class="card-text">${cake.description}</span>
                            <br>
                            <span class="card-text">Create date: <fmt:formatDate value="${cake.createDate}"/></span>
                            <br>
                            <span class="card-text">Expiration date: <fmt:formatDate
                                    value="${cake.expirationDate}"/></span>
                            <p class="card-text font-weight-bold"><fmt:formatNumber value="${cake.price}"
                                                                                    type="currency"/></p>
                            <c:if test="${sessionScope.user == null || sessionScope.user.roleId.roleId != 1}">
                                <a class="btn btn-primary" href="AddToCart?txtCakeId=${cake.cakeId}">Add to card</a>
                            </c:if>

                            <c:if test="${sessionScope.user != null && sessionScope.user.roleId.roleId == 1}">
                                <a class="btn btn-info" href="ViewCakeDetails?txtCakeId=${cake.cakeId}">Update</a>
                                <c:if test="${cake.status}">
                                    <a class="btn btn-warning" href="DeleteCake?txtCakeId=${cake.cakeId}">Disable</a>
                                </c:if>
                                <c:if test="${!cake.status}">
                                    <span class="text-danger">Disabled</span>
                                </c:if>
                            </c:if>

                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </c:if>
    <c:if test="${empty requestScope.cakes}">
        <div class="alert alert-info mx-2 my-2">There is no cake</div>
    </c:if>
</div>

<div class="container">
    <%--Paging--%>
    <div class="text-center">
        <ul class="pagination">
            <c:forEach items="${requestScope.pageIndices}" var="index">
                <li class="page-item">
                    <c:url var="pageLink" value="ViewCake">
                        <c:param name="page" value="${index}"/>
                        <c:param name="txtCategory" value="${param.txtCategory}"/>
                        <c:param name="txtCakeName" value="${param.txtCakeName}"/>
                        <c:param name="txtMinPrice" value="${param.txtMinPrice}"/>
                        <c:param name="txtMaxPrice" value="${param.txtMaxPrice}"/>
                    </c:url>
                    <a class="page-link" href="${pageLink}">${index}</a>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>
</body>
</html>
