/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.provider;

import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;

/**
 * @author duy
 */
public class ImageProvider extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        try {
            String requestURI = request.getRequestURI().replace("%20", " ");
            if (isImageFile(requestURI)) {
                String externalPath = getServletContext().getInitParameter("external-path");
                File file = new File(externalPath, requestURI);
                if (!file.exists()) {
                    file = new File(externalPath, requestURI.substring(requestURI.lastIndexOf("/") + 1));
                }
                if (file.exists()) {
                    // https://stackoverflow.com/questions/51438/getting-a-files-mime-type-in-java
                    String mimeType = URLConnection.guessContentTypeFromName(file.getName());
                    response.setContentType(mimeType);
                    ServletOutputStream outputStream = response.getOutputStream();
                    FileInputStream inputStream = new FileInputStream(file);
                    IOUtils.copy(inputStream, outputStream);
                    inputStream.close();
                    outputStream.close();
                    return;
                } else {
                    log("File does not exist: " + file);
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        response.sendError(404);
    }

    public static boolean isImageFile(String path) {
        path = path.toLowerCase();
        return path.endsWith(".png") || path.endsWith(".jpg")
                || path.endsWith(".jpeg")
                || path.endsWith(".webp");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
