/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.order;

import com.duy.moonshop.jpa.entity.OrderDetail;
import com.duy.moonshop.jpa.entity.Orders;
import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author duy
 */
public class OrderDetailsController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        String url = Constants.ERROR_PAGE;
        try {
            String txtOrderId = request.getParameter("txtOrderId");
            if (txtOrderId != null) {
                int orderId = Integer.parseInt(txtOrderId);
                Users user = (Users) request.getSession(true).getAttribute("user");
                OrderBLO orderBLO = new OrderBLO();
                Orders order = orderBLO.getOrder(user, orderId);
                if (order != null) {
                    request.setAttribute("order", order);
                    List<OrderDetail> orderDetails = orderBLO.getOrderDetails(order);
                    request.setAttribute("orderDetails", orderDetails);
                }
            }
            url = Constants.ORDER_DETAILS_PAGE;
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
