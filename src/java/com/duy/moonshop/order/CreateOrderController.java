/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.order;

import com.duy.moonshop.cart.Cart;
import com.duy.moonshop.jpa.entity.Orders;
import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class CreateOrderController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = Constants.ERROR_PAGE;
        try {
            HttpSession session = request.getSession(true);
            Cart cart = (Cart) session.getAttribute("cart");
            if (cart != null) {
                String userId = getCurrentUserId(request);
                String receiverName = request.getParameter("txtName");
                String phoneNumber = request.getParameter("txtPhoneNumber");
                String shippingAddress = request.getParameter("txtAddress");

                OrderBLO orderBLO = new OrderBLO();
                Orders order = orderBLO.create(cart, userId,
                        receiverName,
                        phoneNumber,
                        shippingAddress);
                if (order != null) {
                    session.removeAttribute("cart");
                    url = Constants.ORDER_DETAILS + "?message=Order success&txtOrderId=" + order.getOrderId();
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        response.sendRedirect(url);
    }

    private String getCurrentUserId(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        Users user = (Users) session.getAttribute("user");
        if (user != null) {
            return user.getUserId();
        }
        return null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
