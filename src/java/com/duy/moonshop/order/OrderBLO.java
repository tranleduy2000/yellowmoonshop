package com.duy.moonshop.order;

import com.duy.moonshop.cart.Cart;
import com.duy.moonshop.jpa.controllers.CakeJpaController;
import com.duy.moonshop.jpa.controllers.OrderDetailJpaController;
import com.duy.moonshop.jpa.controllers.OrdersJpaController;
import com.duy.moonshop.jpa.entity.*;
import com.duy.moonshop.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class OrderBLO {

    private static final int PAYMENT_METHOD_CASH = 0;

    //Payment Status
    //The status of any payment made while you were logged in is available in your My Account page Payment Activity section.
    private static final int PAYMENT_PENDING = 1;
    private static final int PAYMENT_SUCCESS = 2;
    private static final int PAYMENT_FAILED = 3;

    private static final boolean STATUS_ACTIVE = true;

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    public Orders create(Cart cart, String userId, String receiverName, String phoneNumber, String shippingAddress) {
        try {
            refreshDatabase();
            if (!checkStock(cart)) {
                return null;
            }

            // create order
            Orders order = new Orders(0, new Date(System.currentTimeMillis()),
                    STATUS_ACTIVE, PAYMENT_PENDING);
            order.setPaymentMethodId(new Payment(PAYMENT_METHOD_CASH));
            if (userId != null) {
                order.setUserId(new Users(userId));
            }
            order.setReceiverName(receiverName);
            order.setReceiverPhoneNumber(phoneNumber);
            order.setShippingAddress(shippingAddress);

            OrdersJpaController ordersJpaController = new OrdersJpaController(entityManagerFactory);
            ordersJpaController.create(order);
            Integer orderId = order.getOrderId();

            OrderDetailJpaController orderDetailJpaController = new OrderDetailJpaController(entityManagerFactory);
            CakeJpaController cakeJpaController = new CakeJpaController(entityManagerFactory);

            // create order details
            for (Map.Entry<Cake, Integer> pair : cart.getCakes().entrySet()) {
                Integer cakeId = pair.getKey().getCakeId();
                Cake cake = cakeJpaController.findCake(cakeId);
                if (cake.getQuantity() >= pair.getValue()) {

                    OrderDetail orderDetail = new OrderDetail(new OrderDetailPK(orderId, cakeId));
                    orderDetail.setQuantity(pair.getValue());
                    orderDetail.setOrders(order);
                    orderDetail.setCake(cake);
                    orderDetailJpaController.create(orderDetail);

                    cake = cakeJpaController.findCake(cakeId);
                    cake.setQuantity(cake.getQuantity() - pair.getValue());
                    cakeJpaController.edit(cake);
                }
            }

            return ordersJpaController.findOrders(orderId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean checkStock(Cart cart) {
        try {
            refreshDatabase();
            CakeJpaController cakeJpaController = new CakeJpaController(entityManagerFactory);
            for (Map.Entry<Cake, Integer> pair : cart.getCakes().entrySet()) {
                Integer cakeId = pair.getKey().getCakeId();
                Cake cake = cakeJpaController.findCake(cakeId);
                if (cake.getQuantity() < pair.getValue()) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Sync with local database
     */
    private void refreshDatabase() {
        entityManagerFactory.getCache().evictAll();
    }

    public Orders getOrder(Users user, int orderId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Orders> query = entityManager.createQuery("" +
                    "SELECT o FROM Orders o " +
                    "WHERE o.orderId = :orderId AND " +
                    "  ((o.userId IS NULL) OR (o.userId = :userId))", Orders.class);
            query.setParameter("userId", user);
            query.setParameter("orderId", orderId);
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    public List<Orders> getOrders(Users user) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<Orders> query = entityManager.createQuery("" +
                    "SELECT o FROM Orders o " +
                    "WHERE o.userId = :userId " +
                    "ORDER BY o.time DESC ", Orders.class);
            query.setParameter("userId", user);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        } finally {
            entityManager.close();
        }
    }

    public List<OrderDetail> getOrderDetails(Orders order) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            TypedQuery<OrderDetail> query = entityManager.createQuery("" +
                    "SELECT o FROM OrderDetail o " +
                    "WHERE o.orderDetailPK.orderId = :orderId", OrderDetail.class);
            query.setParameter("orderId", order.getOrderId());
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<>();
        } finally {
            entityManager.close();
        }
    }

}
