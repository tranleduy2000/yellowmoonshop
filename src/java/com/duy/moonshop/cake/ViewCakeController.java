/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.cake;

import com.duy.moonshop.jpa.entity.Cake;
import com.duy.moonshop.jpa.entity.Category;
import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.user.blo.UserBLO;
import com.duy.moonshop.utils.Constants;
import com.duy.moonshop.utils.HttpServletRequestExtension;
import com.duy.moonshop.utils.PagingGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author duy
 */
public class ViewCakeController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String url = Constants.ERROR_PAGE;
        try {
            HttpServletRequestExtension reqExt = new HttpServletRequestExtension(getServletContext(), request);
            int pageIndex = reqExt.getIntOrDefault("page", 0);
            String name = reqExt.getStringOrDefault("txtCakeName", "");
            int category = reqExt.getIntOrDefault("txtCategory", -1);
            double minPrice = reqExt.getDoubleOrDefault("txtMinPrice", 0);
            double maxPrice = reqExt.getDoubleOrDefault("txtMaxPrice", Integer.MAX_VALUE);

            boolean onlyActiveCake = false;
            Users user = (Users) request.getSession(true).getAttribute("user");
            if (user != null && user.getRoleId().getRoleId().equals(UserBLO.ADMIN_ROLE)) {
                onlyActiveCake = true;
            }

            CakeBLO cakeBLO = new CakeBLO();
            List<Category> categories = cakeBLO.getCategories();
            List<Cake> cakes = cakeBLO.getCakesSortByDateWithFilters(pageIndex, name,
                    category, minPrice, maxPrice, onlyActiveCake);
            List<Integer> pageIndices = new PagingGenerator().generatePageIndices(pageIndex,
                    cakeBLO.getCakesCountFilterByContent(name,
                            category, minPrice, maxPrice, onlyActiveCake),
                    CakeBLO.NUMBER_OF_CAKES_PER_PAGE);

            request.setAttribute("categories", categories);
            request.setAttribute("cakes", cakes);
            request.setAttribute("currentPage", pageIndex);
            request.setAttribute("pageIndices", pageIndices);

            url = Constants.CAKE_PAGE;
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        request.getRequestDispatcher(url).forward(request, response);
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
