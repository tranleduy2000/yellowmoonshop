package com.duy.moonshop.cake;

import com.duy.moonshop.jpa.controllers.CakeJpaController;
import com.duy.moonshop.jpa.controllers.CategoryJpaController;
import com.duy.moonshop.jpa.controllers.UpdateDetailJpaController;
import com.duy.moonshop.jpa.entity.Cake;
import com.duy.moonshop.jpa.entity.Category;
import com.duy.moonshop.jpa.entity.UpdateDetail;
import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.utils.DbUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public class CakeBLO {
    public static final int NUMBER_OF_CAKES_PER_PAGE = 20;
    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    /**
     * User can find the Cake based on name or range of money or category.
     */
    public List<Cake> getCakesSortByDateWithFilters(int pageIndex, String name, int category,
                                                    double minPrice, double maxPrice,
                                                    boolean adminMode) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager.createQuery("" +
                    "SELECT c FROM Cake c " +
                    "WHERE (:adminMode = true) OR (c.status = true AND c.quantity > 0) " +
                    "   AND (c.name LIKE :name " +
                    "       AND (c.categoryId.categoryId = :categoryId OR :categoryId < 0) " +
                    "       AND :minPrice < c.price AND c.price < :maxPrice) " +
                    "ORDER BY c.createDate DESC ", Cake.class)
                    .setParameter("adminMode", adminMode)
                    .setParameter("name", "%" + name + "%")
                    .setParameter("categoryId", category)
                    .setParameter("minPrice", minPrice)
                    .setParameter("maxPrice", maxPrice)
                    .setMaxResults(NUMBER_OF_CAKES_PER_PAGE)
                    .setFirstResult(pageIndex * NUMBER_OF_CAKES_PER_PAGE)
                    .getResultList();
        } finally {
            entityManager.close();
        }
    }

    public Long getCakesCountFilterByContent(String name, int category,
                                             double minPrice, double maxPrice, boolean adminMode) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            return entityManager.createQuery("" +
                    "SELECT COUNT(c) FROM Cake c " +
                    "WHERE (:adminMode = true) OR (c.status = true AND c.quantity > 0) " +
                    "   AND (c.name LIKE :name " +
                    "       AND (c.categoryId.categoryId = :categoryId OR :categoryId < 0) " +
                    "       AND :minPrice < c.price AND c.price < :maxPrice)", Long.class)
                    .setParameter("adminMode", adminMode)
                    .setParameter("name", "%" + name + "%")
                    .setParameter("categoryId", category)
                    .setParameter("minPrice", minPrice)
                    .setParameter("maxPrice", maxPrice)
                    .getSingleResult();
        } finally {
            entityManager.close();
        }
    }

    public List<Category> getCategories() {
        CategoryJpaController categoryJpaController = new CategoryJpaController(entityManagerFactory);
        return categoryJpaController.findCategoryEntities();
    }

    public boolean create(String name, String description, String imagePath, double price, Date createDate,
                          Date expirationDate, int categoryId, int quantity) {
        try {
            CakeJpaController cakeJpaController = new CakeJpaController(entityManagerFactory);
            Cake cake = new Cake(0, name, BigDecimal.valueOf(price),
                    createDate, expirationDate, quantity, true);
            cake.setImagePath(imagePath);
            cake.setDescription(description);
            cake.setCategoryId(new Category(categoryId));
            cakeJpaController.create(cake);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deactive(int cakeId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            boolean result = entityManager.createQuery("UPDATE Cake SET status = false WHERE cakeId = :cakeId")
                    .setParameter("cakeId", cakeId)
                    .executeUpdate() > 0;
            entityManager.getTransaction().commit();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return false;
    }

    public Cake get(int cakeId) {
        return new CakeJpaController(entityManagerFactory).findCake(cakeId);
    }

    public boolean update(Users user, int cakeId, String name, String description, String imagePath, double price, Date createDate,
                          Date expirationDate, int categoryId, int quantity, boolean status) {
        try {
            CakeJpaController cakeJpaController = new CakeJpaController(entityManagerFactory);
            Cake cake = cakeJpaController.findCake(cakeId);
            if (cake != null) {
                cake.setName(name);
                cake.setDescription(description);
                if (imagePath != null) {
                    cake.setImagePath(imagePath);
                }
                cake.setPrice(BigDecimal.valueOf(price));
                cake.setCreateDate(createDate);
                cake.setExpirationDate(expirationDate);
                cake.setCategoryId(new Category(categoryId));
                cake.setQuantity(quantity);
                cake.setStatus(status);
                cakeJpaController.edit(cake);

                UpdateDetailJpaController updateDetailJpaController = new UpdateDetailJpaController(entityManagerFactory);
                UpdateDetail updateDetail = new UpdateDetail(0, new Date(System.currentTimeMillis()));
                updateDetail.setCakeId(cake);
                updateDetail.setUserId(user);
                updateDetailJpaController.create(updateDetail);

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
