/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.cake;

import com.duy.moonshop.jpa.entity.Category;
import com.duy.moonshop.utils.Constants;
import com.duy.moonshop.utils.HttpServletRequestExtension;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author duy
 */
public class CreateCakeController extends HttpServlet {

    protected void processRequest(HttpServletRequest r, HttpServletResponse response)
            throws ServletException, IOException {
        HttpServletRequestExtension request = new HttpServletRequestExtension(getServletContext(), r);

        String url = Constants.ERROR_PAGE;
        boolean forwarding = true;
        try {
            CakeBLO cakeBLO = new CakeBLO();
            List<Category> categories = cakeBLO.getCategories();
            r.setAttribute("categories", categories);
            if (request.getParameter("btnAction") == null) {
                url = Constants.CREATE_CAKE_PAGE;

            } else {

                CakeError error = new CakeError();
                boolean isError = false;

                String name = request.getParameter("txtName");
                String imagePath = request.getParameter("txtImageFile");
                String description = request.getParameter("txtDescription");
                double price = request.getDoubleOrDefault("txtPrice", 0);
                java.sql.Date createDate = request.getDate("txtCreateDate");
                java.sql.Date expirationDate = request.getDate("txtExpirationDate");
                int categoryId = request.getIntOrDefault("cbxCategoryId", -1);
                int quantity = request.getIntOrDefault("txtQuantity", -1);

                r.setAttribute("txtName", name);
                r.setAttribute("txtDescription", description);
                r.setAttribute("cbxCategoryId", categoryId);
                r.setAttribute("txtPrice", price);
                r.setAttribute("txtQuantity", quantity);

                if (name.isEmpty()) {
                    isError = true;
                    error.setNameError("Name must be not empty");
                }
                if (price <= 0) {
                    isError = true;
                    error.setPriceError("Price must > 0");
                }
                if (createDate == null) {
                    isError = true;
                    error.setCreateDateError("Invalid created date");
                }
                if (expirationDate == null) {
                    isError = true;
                    error.setExpirationDateError("Invalid expiration date");
                }
                if (createDate != null && expirationDate != null && createDate.compareTo(expirationDate) > 0) {
                    isError = true;
                    error.setExpirationDateError("create date must be less than expirationDate");
                }
                if (categoryId < 0) {
                    isError = true;
                    error.setCategoryIdError("Invalid category");
                }
                if (quantity < 0) {
                    isError = true;
                    error.setQuantityError("Quantity must be >= 0");
                }

                if (isError) {
                    r.setAttribute("error", error);
                    url = Constants.CREATE_CAKE_PAGE;
                    forwarding = true;

                } else {
                    if (cakeBLO.create(name, description, imagePath, price,
                            createDate, expirationDate, categoryId, quantity)) {
                        url = Constants.HOME;
                    } else {
                        url = Constants.ERROR_PAGE;
                    }
                    forwarding = false;
                }

            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        if (forwarding) {
            r.getRequestDispatcher(url).forward(r, response);
        } else {
            response.sendRedirect(url);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
