/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.cake;

import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.user.blo.UserBLO;
import com.duy.moonshop.utils.Constants;
import com.duy.moonshop.utils.HttpServletRequestExtension;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author duy
 */
public class UpdateCakeController extends HttpServlet {

    protected void processRequest(HttpServletRequest r, HttpServletResponse response)
            throws ServletException, IOException {
        HttpServletRequestExtension request = new HttpServletRequestExtension(getServletContext(), r);

        String url = Constants.ERROR_PAGE;
        try {
            Users user = (Users) r.getSession(true).getAttribute("user");
            if (user == null || !user.getRoleId().getRoleId().equals(UserBLO.ADMIN_ROLE)) {
                url = Constants.LOGIN_PAGE;
            } else {

                CakeBLO cakeBLO = new CakeBLO();
                CakeError error = new CakeError();
                boolean isError = false;

                int cakeId = Integer.parseInt(request.getParameter("txtCakeId"));
                String name = request.getParameter("txtName");
                String imagePath = request.getParameter("txtImageFile");
                String description = request.getParameter("txtDescription");
                double price = request.getDoubleOrDefault("txtPrice", 0);
                java.sql.Date createDate = request.getDate("txtCreateDate");
                java.sql.Date expirationDate = request.getDate("txtExpirationDate");
                int categoryId = request.getIntOrDefault("cbxCategoryId", -1);
                int quantity = request.getIntOrDefault("txtQuantity", -1);
                boolean status = request.getBooleanOrDefault("checkBoxStatus", true);

                if (name.isEmpty()) {
                    isError = true;
                    error.setNameError("Name must be not empty");
                }
                if (price <= 0) {
                    isError = true;
                    error.setPriceError("Price must > 0");
                }
                if (createDate == null) {
                    isError = true;
                    error.setCreateDateError("Invalid created date");
                }
                if (expirationDate == null) {
                    isError = true;
                    error.setExpirationDateError("Invalid expiration date");
                }
                if (createDate != null && expirationDate != null && createDate.compareTo(expirationDate) > 0) {
                    isError = true;
                    error.setExpirationDateError("create date must be less than expirationDate");
                }
                if (categoryId < 0) {
                    isError = true;
                    error.setCategoryIdError("Invalid category");
                }
                if (quantity < 0) {
                    isError = true;
                    error.setQuantityError("Quantity must be >= 0");
                }

                if (isError) {
                    r.setAttribute("error", error);
                    url = Constants.CAKE_DETAILS + "?txtCakeId=" + cakeId;

                } else {
                    if (cakeBLO.update(user, cakeId, name, description, imagePath, price,
                            createDate, expirationDate, categoryId, quantity, status)) {
                        url = Constants.CAKE_DETAILS + "?txtCakeId=" + cakeId + "&message=Update success";
                    } else {
                        url = Constants.ERROR_PAGE;
                    }
                }
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        r.getRequestDispatcher(url).forward(r, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
