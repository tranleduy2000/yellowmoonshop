package com.duy.moonshop.cart;

import com.duy.moonshop.jpa.entity.Cake;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Cart implements Serializable {

    private Map<Cake, Integer> cakes = new HashMap<>();

    public void addCake(Cake cake) {
        Integer quantity = cakes.getOrDefault(cake, 0);
        cakes.put(cake, quantity + 1);
    }

    public void removeCake(Cake cake, int quantity) {
        Integer current = cakes.getOrDefault(cake, 0);
        current -= quantity;
        if (current <= 0) {
            cakes.remove(cake);
        } else {
            cakes.put(cake, current);
        }
    }

    public int size() {
        return cakes.size();
    }

    public Map<Cake, Integer> getCakes() {
        return cakes;
    }

    public void setCakes(Map<Cake, Integer> cakes) {
        this.cakes = cakes;
    }

    public int get(Cake cake) {
        return cakes.getOrDefault(cake, 0);
    }

    public float getTotal() {
        float total = 0;
        for (Map.Entry<Cake, Integer> pair : cakes.entrySet()) {
            total += pair.getKey().getPrice().floatValue() * pair.getValue();
        }
        return total;
    }
}
