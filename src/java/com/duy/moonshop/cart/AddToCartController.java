/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.cart;

import com.duy.moonshop.cake.CakeBLO;
import com.duy.moonshop.jpa.entity.Cake;
import com.duy.moonshop.utils.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class AddToCartController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String url = Constants.ERROR_PAGE;
        try {
            int cakeId = Integer.parseInt(request.getParameter("txtCakeId"));
            String destination = request.getParameter("destination"); // cake.jsp or cart.jsp
            if (destination == null) {
                destination = Constants.CAKE;
            }

            HttpSession session = request.getSession(true);
            Cart cart = (Cart) session.getAttribute("cart");
            if (cart == null) {
                cart = new Cart();
                session.setAttribute("cart", cart);
            }

            CakeBLO cakeBLO = new CakeBLO();
            Cake cake = cakeBLO.get(cakeId);
            if (cake != null) {
                if (cart.get(cake) >= cake.getQuantity()) {
                    url = destination + "?error=Number of cake exceeds the limit";
                } else {
                    cart.addCake(cake);
                    url = destination + "?message=Added " + cake.getName() + " to cart";
                }
            }

        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        response.sendRedirect(url);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
