/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop;

import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.user.LoginController;
import com.duy.moonshop.user.blo.UserBLO;
import com.duy.moonshop.utils.Constants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * @author duy
 */
public class MainController implements Filter {

    private static final boolean debug = true;

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured.
    private FilterConfig filterConfig = null;

    private final Set<String> unrestrictedResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<String> userResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
    private final Set<String> adminResources = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    /**
     * Login -> LoginController
     * Logout -> ExitController
     */
    private final Map<String, String> mappedResources = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    public MainController() {
        // No if else statements
        mappedResources.put("Login", LoginController.class.getSimpleName());
        mappedResources.put("CreateAccount", Constants.CREATE_ACCOUNT);
        mappedResources.put("Logout", Constants.LOGOUT);
        mappedResources.put("CreateCake", Constants.CREATE_CAKE);
        mappedResources.put("ViewCake", Constants.CAKE);
        mappedResources.put("Search Cake", Constants.CAKE);
        mappedResources.put("DeleteCake", Constants.DELETE_CAKE);
        mappedResources.put("AddToCart", Constants.ADD_TO_CART);
        mappedResources.put("RemoveFromCart", Constants.REMOVE_FROM_CART);
        mappedResources.put("MakePayment", Constants.CREATE_ORDER);
        mappedResources.put("Checkout", Constants.CHECKOUT);
        mappedResources.put("TrackOrders", Constants.ORDER);
        mappedResources.put("ViewOrderDetails", Constants.ORDER_DETAILS);
        mappedResources.put("UpdateAccount", Constants.UPDATE_ACCOUNT);
        mappedResources.put("UpdatePassword", Constants.UPDATE_PASSWORD);
        mappedResources.put("LoginWithGoogle", Constants.GOOGLE_LOGIN);
        mappedResources.put("ViewCakeDetails", Constants.CAKE_DETAILS);
        mappedResources.put("UpdateCake", Constants.UPDATE_CAKE);

        unrestrictedResources.add(""); // root of domain
        unrestrictedResources.add(Constants.LOGIN_PAGE);
        unrestrictedResources.add(Constants.LOGIN);
        unrestrictedResources.add(Constants.LOGOUT);
        unrestrictedResources.add(Constants.ERROR_PAGE);
        unrestrictedResources.add(Constants.CREATE_ACCOUNT_PAGE);
        unrestrictedResources.add(Constants.CREATE_ACCOUNT);
        unrestrictedResources.add(Constants.CAKE_PAGE);
        unrestrictedResources.add(Constants.CAKE);
        unrestrictedResources.add(Constants.ADD_TO_CART);
        unrestrictedResources.add(Constants.CART_PAGE);
        unrestrictedResources.add(Constants.REMOVE_FROM_CART);
        unrestrictedResources.add(Constants.CREATE_ORDER);
        unrestrictedResources.add(Constants.CHECKOUT);
        unrestrictedResources.add(Constants.CHECKOUT_PAGE);
        unrestrictedResources.add(Constants.ORDER_DETAILS_PAGE);
        unrestrictedResources.add(Constants.ORDER_DETAILS);
        unrestrictedResources.add(Constants.GOOGLE_LOGIN);

        userResources.add(Constants.ORDER);
        userResources.add(Constants.ORDER_PAGE);
        userResources.add(Constants.PROFILE_PAGE);
        userResources.add(Constants.UPDATE_ACCOUNT);
        userResources.add(Constants.UPDATE_PASSWORD_PAGE);
        userResources.add(Constants.UPDATE_PASSWORD);

        adminResources.add(Constants.CREATE_CAKE_PAGE);
        adminResources.add(Constants.CREATE_CAKE);
        adminResources.add(Constants.DELETE_CAKE);
        adminResources.add(Constants.PROFILE_PAGE);
        adminResources.add(Constants.UPDATE_ACCOUNT);
        adminResources.add(Constants.UPDATE_PASSWORD_PAGE);
        adminResources.add(Constants.UPDATE_PASSWORD);
        adminResources.add(Constants.CAKE_DETAILS);
        adminResources.add(Constants.CAKE_DETAILS_PAGE);
        adminResources.add(Constants.UPDATE_CAKE);
    }


    /**
     * @param req   The servlet request we are processing
     * @param res   The servlet res we are creating
     * @param chain The filter chain we are processing
     */
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) {
        try {
            final HttpServletRequest request = (HttpServletRequest) req;
            final HttpServletResponse response = (HttpServletResponse) res;

            String resource = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/") + 1);
            log("resource: " + resource);

            // forward to appropriated resource. Example: Login -> LoginController, Abc -> xyz.jsp
            String mappedResource = mappedResources.get(resource);
            if (mappedResource != null) {
                log("mappedResource: " + mappedResource + ". Forwarding...");
                request.getRequestDispatcher(mappedResource).forward(request, response);
                return;
            }

            if (isUnrestrictedResource(resource)) {
                log("isUnrestrictedResource");
                chain.doFilter(request, res);
                return;
            }

            Users user = (Users) request.getSession(true).getAttribute("user");
            if (user == null) { // check login
                log("!!! Access denied");
                response.sendRedirect(Constants.LOGIN_PAGE);
                return;
            }

            if (user.getRoleId().getRoleId().equals(UserBLO.USER_ROLE)) { // check user role
                if (userResources.contains(resource)) {
                    log("Access granted");
                    chain.doFilter(request, res);
                    return;
                }

            } else if (user.getRoleId().getRoleId().equals(UserBLO.ADMIN_ROLE)) { // check admin role
                if (adminResources.contains(resource)) {
                    log("Access granted");
                    chain.doFilter(request, res);
                    return;
                }
            }

            log("!!! Access denied");
            response.sendRedirect(Constants.ERROR_PAGE); // invalid request

        } catch (Exception e) {
            log(e.getMessage());
        }
    }

    private boolean isUnrestrictedResource(String resource) {
        return unrestrictedResources.contains(resource) || resource.matches("(.*?)\\.(js|css|png|jpeg|jpg)");
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        if (filterConfig != null) {
            if (debug) {
                log("MainController:Initializing filter");
            }
        }
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("MainController()");
        }
        StringBuffer sb = new StringBuffer("MainController(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

}
