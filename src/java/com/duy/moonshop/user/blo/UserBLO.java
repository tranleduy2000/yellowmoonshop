package com.duy.moonshop.user.blo;

import com.duy.moonshop.jpa.controllers.UsersJpaController;
import com.duy.moonshop.jpa.entity.Role;
import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.utils.DbUtils;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.nio.charset.StandardCharsets;

public class UserBLO {

    public static final Integer USER_ROLE = 0;
    public static final Integer ADMIN_ROLE = 1;

    public static final boolean STATUS_ACTIVE = true;

    private final EntityManagerFactory entityManagerFactory = DbUtils.getInstance();

    public Users checkLogin(String userId, String password) {
        refreshDatabase();

        String hashedPassword = DigestUtils.sha256Hex(password.getBytes(StandardCharsets.UTF_8));
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        String jpql = "SELECT u FROM Users u WHERE u.userId = :userId AND u.password = :password";
        TypedQuery<Users> query = entityManager.createQuery(jpql, Users.class);
        query.setParameter("userId", userId);
        query.setParameter("password", hashedPassword);
        try {
            Users user = query.getSingleResult();
            user.setPassword("");
            return user;
        } catch (NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    private void refreshDatabase() {
        entityManagerFactory.getCache().evictAll();
    }

    public Users get(String userId) {
        refreshDatabase();
        UsersJpaController controller = new UsersJpaController(entityManagerFactory);
        try {
            Users user = controller.findUsers(userId);
            user.setPassword(""); // remove password
            return user;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean insert(String userId, String password, String fullName, String phoneNumber, boolean thirdPartyLogin) {
        try {
            final String finalPassword;
            if (thirdPartyLogin) {
                finalPassword = "";
            } else {
                finalPassword = DigestUtils.sha256Hex(password);
            }

            Users user = new Users(userId, fullName, finalPassword, STATUS_ACTIVE);
            user.setRoleId(new Role(USER_ROLE));
            user.setPhoneNumber(phoneNumber);

            UsersJpaController usersJpaController = new UsersJpaController(entityManagerFactory);
            usersJpaController.create(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(String userId, String fullName, String phoneNumber) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery("" +
                    "UPDATE Users " +
                    "SET fullName = :fullName, phoneNumber = :phoneNumber " +
                    "WHERE userId = :userId")
                    .setParameter("userId", userId)
                    .setParameter("fullName", fullName)
                    .setParameter("phoneNumber", phoneNumber)
                    .executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

    public boolean updatePassword(String userId, String oldPassword, String newPassword) {
        String hashedOldPassword = DigestUtils.sha256Hex(oldPassword);
        String hashedNewPassword = DigestUtils.sha256Hex(newPassword);

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            int result = entityManager.createQuery(
                    "UPDATE Users SET password = :hashedNewPassword " +
                            "WHERE userId = :userId AND password = :hashedOldPassword")
                    .setParameter("userId", userId)
                    .setParameter("hashedOldPassword", hashedOldPassword)
                    .setParameter("hashedNewPassword", hashedNewPassword).executeUpdate();
            entityManager.getTransaction().commit();
            return result > 0;
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
    }

}
