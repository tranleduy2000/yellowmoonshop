/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.user;


import com.duy.moonshop.jpa.entity.Users;
import com.duy.moonshop.user.blo.UserBLO;
import com.duy.moonshop.utils.Constants;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author duy
 */
public class LoginController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        String urlRewriting = Constants.LOGIN_PAGE;
        try {
            String userId = request.getParameter("txtUserId");
            String password = request.getParameter("txtPassword");

            UserBLO userBLO = new UserBLO();
            Users user = userBLO.checkLogin(userId, password);
            boolean valid = user != null;
            log("Account exist: " + valid);

            if (valid) {
                HttpSession session = request.getSession(true);
                session.setAttribute("user", user);
                urlRewriting = Constants.CAKE;
            } else {
                urlRewriting = Constants.LOGIN_PAGE + "?error=Invalid user name or password";
            }
        } catch (Exception e) {
            log(e.getMessage(), e);
        }
        response.sendRedirect(urlRewriting);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
