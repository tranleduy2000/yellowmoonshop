package com.duy.moonshop.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DbUtils {
    private static final String PERSISTENCE_UNIT_NAME = "YellowMoonShopPU";
    private static EntityManagerFactory entityManagerFactory;

    public static EntityManagerFactory getInstance() {
        synchronized (DbUtils.class) {
            if (entityManagerFactory == null) {
                entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
            }
        }
        return entityManagerFactory;
    }
}
