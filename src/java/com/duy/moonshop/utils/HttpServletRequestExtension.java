package com.duy.moonshop.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class HttpServletRequestExtension {

    private static final AtomicLong counter = new AtomicLong(System.currentTimeMillis());

    private final ServletContext servletContext;
    private final HttpServletRequest request;
    private List<FileItem> fileItems;

    public HttpServletRequestExtension(ServletContext servletContext, HttpServletRequest request) {
        this.servletContext = servletContext;
        this.request = request;
    }

    public int getIntOrDefault(String key, int defaultValue) {
        try {
            return Integer.parseInt(getParameter(key));
        } catch (Exception e) {
        }
        return defaultValue;
    }

    public String getStringOrDefault(String key, String defaultValue) throws Exception {
        String parameter = getParameter(key);
        if (parameter != null) {
            return parameter;
        }
        return defaultValue;
    }

    public double getDoubleOrDefault(String key, double defaultValue) {
        try {
            return Double.parseDouble(getParameter(key));
        } catch (Exception e) {
        }
        return defaultValue;
    }


    public String getParameter(String name) throws Exception {
        if (ServletFileUpload.isMultipartContent(request)) {
            if (fileItems == null) {
                fileItems = parseRequest(request);
            }
            return getParameter(fileItems, name);
        }
        return request.getParameter(name);
    }

    /**
     * @return If parameter is a normal format, return its values. If parameter is a file, return relative path of this file.
     */
    private String getParameter(List<FileItem> fileItems, String parameterName) throws Exception {
        for (FileItem fileItem : fileItems) {
            if (fileItem.isFormField()) { // normal parameter
                if (fileItem.getFieldName().equals(parameterName)) {
                    return fileItem.getString();
                }
            } else {  // binary parameter
                if (fileItem.getFieldName().equalsIgnoreCase(parameterName)) {
                    return getFileFromClient(fileItem);
                }
            }
        }
        return null;
    }


    public List<FileItem> parseRequest(HttpServletRequest request) throws FileUploadException {
        final int oneMegabytes = 1024 * 1024;
        final int maxFileSize = 10 * oneMegabytes;
        final int maxMemSize = 4 * oneMegabytes;

        DiskFileItemFactory factory = new DiskFileItemFactory();
        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);
        // Location to save data that is larger than maxMemSize.
        File repository = new File(getServletContext().getRealPath("") + "/temp/");
        repository.mkdirs();
        factory.setRepository(repository);
        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);
        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        // Parse the request to get file items.
        return upload.parseRequest(request);
    }

    /**
     * externalPath: the directory outside tomcat webapps directory
     * contextPath: current webapps directory
     *
     * @return relative path.
     * Example:
     * - ExternalPath: C:\Webapp\
     * - Absolute path of a file: C:\Webapp\Images\img1.png
     * - Relative path: \Images\img1.png
     */
    private String getFileFromClient(FileItem fileItem) throws Exception {
        if (fileItem.getSize() <= 0) { // user does not select any file
            return null;
        }
        //  The directory outside tomcat webapps directory
        String externalPath = getServletContext().getInitParameter("external-path");

        String fileName = fileItem.getName();
        String fileExt = getExt(fileName, "jpg");
        String hashedName = DigestUtils.sha256Hex(fileName) + counter.getAndIncrement() + "." + fileExt;

        File uploadedFile = new File(externalPath, hashedName);
        uploadedFile.getParentFile().mkdirs();
        getServletContext().log("Upload file to: " + uploadedFile.getAbsolutePath());
        fileItem.write(uploadedFile);
        return hashedName;
    }

    private String getExt(String fileName, String defaultVal) {
        if (fileName.contains(".")) {
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        }
        return defaultVal;
    }

    public ServletContext getServletContext() {
        return servletContext;
    }

    public java.sql.Date getDate(String name) throws Exception {
        String parameter = getParameter(name);
        if (parameter == null) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Date date = format.parse(parameter);
        date.setHours(today.getHours());
        date.setMinutes(today.getMinutes());
        date.setSeconds(today.getSeconds());
        return new java.sql.Date(date.getTime());
    }

    public boolean getBooleanOrDefault(String name, boolean defValue) throws Exception {
        String parameter = getParameter(name);
        if (parameter != null) {
            if (parameter.equals("0") || parameter.equals("no")) {
                return false;
            }
            if (parameter.equals("1") || parameter.equals("yes")) {
                return true;
            }
            return Boolean.parseBoolean(parameter);
        }
        return defValue;
    }
}
