package com.duy.moonshop.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PagingGenerator {

    public List<Integer> generatePageIndices(int currentPage, long numberOfEntities, int numberOfEntitiesPerPage) {
        int numberOfPages = (int) Math.ceil(numberOfEntities * 1.0f / numberOfEntitiesPerPage);
        if (numberOfPages <= 1) {
            return new ArrayList<>();
        }
        if (currentPage == 0) {
            return Arrays.asList(0, 1);
        }
        if (currentPage == numberOfPages - 1) {
            return Arrays.asList(currentPage - 1, currentPage);
        }
        return Arrays.asList(currentPage - 1, currentPage, currentPage + 1);
    }
}
