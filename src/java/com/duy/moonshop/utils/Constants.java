package com.duy.moonshop.utils;

import com.duy.moonshop.cake.*;
import com.duy.moonshop.cart.AddToCartController;
import com.duy.moonshop.cart.RemoveFromCartController;
import com.duy.moonshop.order.CheckoutController;
import com.duy.moonshop.order.CreateOrderController;
import com.duy.moonshop.order.OrderController;
import com.duy.moonshop.order.OrderDetailsController;
import com.duy.moonshop.user.*;

public class Constants {
    public static final String LOGIN_PAGE = "login.jsp";
    public static final String LOGIN = LoginController.class.getSimpleName();
    public static final String LOGOUT = LogoutController.class.getSimpleName();

    public static final String CAKE_PAGE = "cake.jsp";
    public static final String CAKE = ViewCakeController.class.getSimpleName();
    public static final String DELETE_CAKE = DeleteCakeController.class.getSimpleName();

    public static final String ERROR_PAGE = "error.jsp";

    public static final String PROFILE_PAGE = "userDetails.jsp";

    public static final String CREATE_ACCOUNT_PAGE = "createAccount.jsp";
    public static final String CREATE_ACCOUNT = CreateAccountController.class.getSimpleName();

    public static final String CREATE_CAKE_PAGE = "createCake.jsp";
    public static final String CREATE_CAKE = CreateCakeController.class.getSimpleName();

    public static final String HOME = CAKE;
    public static final String CART_PAGE = "cart.jsp";
    public static final String ADD_TO_CART = AddToCartController.class.getSimpleName();
    public static final String REMOVE_FROM_CART = RemoveFromCartController.class.getSimpleName();
    public static final String CREATE_ORDER = CreateOrderController.class.getSimpleName();
    public static final String CHECKOUT = CheckoutController.class.getSimpleName();
    public static final String CHECKOUT_PAGE = "checkout.jsp";
    public static final String ORDER_DETAILS = OrderDetailsController.class.getSimpleName();
    public static final String ORDER_DETAILS_PAGE = "orderDetails.jsp";
    public static final String ORDER = OrderController.class.getSimpleName();
    public static final String ORDER_PAGE = "orders.jsp";
    public static final String UPDATE_ACCOUNT = UpdateAccountController.class.getSimpleName();
    public static final String UPDATE_PASSWORD_PAGE = "updatePassword.jsp";
    public static final String UPDATE_PASSWORD = UpdatePasswordController.class.getSimpleName();
    public static final String GOOGLE_LOGIN = GoogleLoginController.class.getSimpleName();
    public static final String CAKE_DETAILS = CakeDetailsController.class.getSimpleName();
    public static final String CAKE_DETAILS_PAGE = "updateCake.jsp";
    public static final String UPDATE_CAKE = UpdateCakeController.class.getSimpleName();
}
