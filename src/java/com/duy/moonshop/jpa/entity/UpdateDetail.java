/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.jpa.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author duy
 */
@Entity
@Table(name = "UpdateDetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UpdateDetail.findAll", query = "SELECT u FROM UpdateDetail u"),
    @NamedQuery(name = "UpdateDetail.findById", query = "SELECT u FROM UpdateDetail u WHERE u.id = :id"),
    @NamedQuery(name = "UpdateDetail.findByDate", query = "SELECT u FROM UpdateDetail u WHERE u.date = :date")})
public class UpdateDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @JoinColumn(name = "cakeId", referencedColumnName = "cakeId")
    @ManyToOne(optional = false)
    private Cake cakeId;
    @JoinColumn(name = "userId", referencedColumnName = "userId")
    @ManyToOne(optional = false)
    private Users userId;

    public UpdateDetail() {
    }

    public UpdateDetail(Integer id) {
        this.id = id;
    }

    public UpdateDetail(Integer id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Cake getCakeId() {
        return cakeId;
    }

    public void setCakeId(Cake cakeId) {
        this.cakeId = cakeId;
    }

    public Users getUserId() {
        return userId;
    }

    public void setUserId(Users userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UpdateDetail)) {
            return false;
        }
        UpdateDetail other = (UpdateDetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.duy.moonshop.jpa.entity.UpdateDetail[ id=" + id + " ]";
    }
    
}
