/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.jpa.controllers;

import com.duy.moonshop.exceptions.NonexistentEntityException;
import com.duy.moonshop.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.moonshop.jpa.entity.Cake;
import com.duy.moonshop.jpa.entity.OrderDetail;
import com.duy.moonshop.jpa.entity.OrderDetailPK;
import com.duy.moonshop.jpa.entity.Orders;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class OrderDetailJpaController implements Serializable {

    public OrderDetailJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OrderDetail orderDetail) throws PreexistingEntityException, Exception {
        if (orderDetail.getOrderDetailPK() == null) {
            orderDetail.setOrderDetailPK(new OrderDetailPK());
        }
        orderDetail.getOrderDetailPK().setOrderId(orderDetail.getOrders().getOrderId());
        orderDetail.getOrderDetailPK().setCakeId(orderDetail.getCake().getCakeId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cake cake = orderDetail.getCake();
            if (cake != null) {
                cake = em.getReference(cake.getClass(), cake.getCakeId());
                orderDetail.setCake(cake);
            }
            Orders orders = orderDetail.getOrders();
            if (orders != null) {
                orders = em.getReference(orders.getClass(), orders.getOrderId());
                orderDetail.setOrders(orders);
            }
            em.persist(orderDetail);
            if (cake != null) {
                cake.getOrderDetailCollection().add(orderDetail);
                cake = em.merge(cake);
            }
            if (orders != null) {
                orders.getOrderDetailCollection().add(orderDetail);
                orders = em.merge(orders);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findOrderDetail(orderDetail.getOrderDetailPK()) != null) {
                throw new PreexistingEntityException("OrderDetail " + orderDetail + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OrderDetail orderDetail) throws NonexistentEntityException, Exception {
        orderDetail.getOrderDetailPK().setOrderId(orderDetail.getOrders().getOrderId());
        orderDetail.getOrderDetailPK().setCakeId(orderDetail.getCake().getCakeId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OrderDetail persistentOrderDetail = em.find(OrderDetail.class, orderDetail.getOrderDetailPK());
            Cake cakeOld = persistentOrderDetail.getCake();
            Cake cakeNew = orderDetail.getCake();
            Orders ordersOld = persistentOrderDetail.getOrders();
            Orders ordersNew = orderDetail.getOrders();
            if (cakeNew != null) {
                cakeNew = em.getReference(cakeNew.getClass(), cakeNew.getCakeId());
                orderDetail.setCake(cakeNew);
            }
            if (ordersNew != null) {
                ordersNew = em.getReference(ordersNew.getClass(), ordersNew.getOrderId());
                orderDetail.setOrders(ordersNew);
            }
            orderDetail = em.merge(orderDetail);
            if (cakeOld != null && !cakeOld.equals(cakeNew)) {
                cakeOld.getOrderDetailCollection().remove(orderDetail);
                cakeOld = em.merge(cakeOld);
            }
            if (cakeNew != null && !cakeNew.equals(cakeOld)) {
                cakeNew.getOrderDetailCollection().add(orderDetail);
                cakeNew = em.merge(cakeNew);
            }
            if (ordersOld != null && !ordersOld.equals(ordersNew)) {
                ordersOld.getOrderDetailCollection().remove(orderDetail);
                ordersOld = em.merge(ordersOld);
            }
            if (ordersNew != null && !ordersNew.equals(ordersOld)) {
                ordersNew.getOrderDetailCollection().add(orderDetail);
                ordersNew = em.merge(ordersNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                OrderDetailPK id = orderDetail.getOrderDetailPK();
                if (findOrderDetail(id) == null) {
                    throw new NonexistentEntityException("The orderDetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(OrderDetailPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OrderDetail orderDetail;
            try {
                orderDetail = em.getReference(OrderDetail.class, id);
                orderDetail.getOrderDetailPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orderDetail with id " + id + " no longer exists.", enfe);
            }
            Cake cake = orderDetail.getCake();
            if (cake != null) {
                cake.getOrderDetailCollection().remove(orderDetail);
                cake = em.merge(cake);
            }
            Orders orders = orderDetail.getOrders();
            if (orders != null) {
                orders.getOrderDetailCollection().remove(orderDetail);
                orders = em.merge(orders);
            }
            em.remove(orderDetail);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<OrderDetail> findOrderDetailEntities() {
        return findOrderDetailEntities(true, -1, -1);
    }

    public List<OrderDetail> findOrderDetailEntities(int maxResults, int firstResult) {
        return findOrderDetailEntities(false, maxResults, firstResult);
    }

    private List<OrderDetail> findOrderDetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(OrderDetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OrderDetail findOrderDetail(OrderDetailPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OrderDetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getOrderDetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<OrderDetail> rt = cq.from(OrderDetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
