/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.jpa.controllers;

import com.duy.moonshop.exceptions.IllegalOrphanException;
import com.duy.moonshop.exceptions.NonexistentEntityException;
import com.duy.moonshop.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.moonshop.jpa.entity.Role;
import com.duy.moonshop.jpa.entity.Orders;
import java.util.ArrayList;
import java.util.Collection;
import com.duy.moonshop.jpa.entity.UpdateDetail;
import com.duy.moonshop.jpa.entity.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class UsersJpaController implements Serializable {

    public UsersJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Users users) throws PreexistingEntityException, Exception {
        if (users.getOrdersCollection() == null) {
            users.setOrdersCollection(new ArrayList<Orders>());
        }
        if (users.getUpdateDetailCollection() == null) {
            users.setUpdateDetailCollection(new ArrayList<UpdateDetail>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Role roleId = users.getRoleId();
            if (roleId != null) {
                roleId = em.getReference(roleId.getClass(), roleId.getRoleId());
                users.setRoleId(roleId);
            }
            Collection<Orders> attachedOrdersCollection = new ArrayList<Orders>();
            for (Orders ordersCollectionOrdersToAttach : users.getOrdersCollection()) {
                ordersCollectionOrdersToAttach = em.getReference(ordersCollectionOrdersToAttach.getClass(), ordersCollectionOrdersToAttach.getOrderId());
                attachedOrdersCollection.add(ordersCollectionOrdersToAttach);
            }
            users.setOrdersCollection(attachedOrdersCollection);
            Collection<UpdateDetail> attachedUpdateDetailCollection = new ArrayList<UpdateDetail>();
            for (UpdateDetail updateDetailCollectionUpdateDetailToAttach : users.getUpdateDetailCollection()) {
                updateDetailCollectionUpdateDetailToAttach = em.getReference(updateDetailCollectionUpdateDetailToAttach.getClass(), updateDetailCollectionUpdateDetailToAttach.getId());
                attachedUpdateDetailCollection.add(updateDetailCollectionUpdateDetailToAttach);
            }
            users.setUpdateDetailCollection(attachedUpdateDetailCollection);
            em.persist(users);
            if (roleId != null) {
                roleId.getUsersCollection().add(users);
                roleId = em.merge(roleId);
            }
            for (Orders ordersCollectionOrders : users.getOrdersCollection()) {
                Users oldUserIdOfOrdersCollectionOrders = ordersCollectionOrders.getUserId();
                ordersCollectionOrders.setUserId(users);
                ordersCollectionOrders = em.merge(ordersCollectionOrders);
                if (oldUserIdOfOrdersCollectionOrders != null) {
                    oldUserIdOfOrdersCollectionOrders.getOrdersCollection().remove(ordersCollectionOrders);
                    oldUserIdOfOrdersCollectionOrders = em.merge(oldUserIdOfOrdersCollectionOrders);
                }
            }
            for (UpdateDetail updateDetailCollectionUpdateDetail : users.getUpdateDetailCollection()) {
                Users oldUserIdOfUpdateDetailCollectionUpdateDetail = updateDetailCollectionUpdateDetail.getUserId();
                updateDetailCollectionUpdateDetail.setUserId(users);
                updateDetailCollectionUpdateDetail = em.merge(updateDetailCollectionUpdateDetail);
                if (oldUserIdOfUpdateDetailCollectionUpdateDetail != null) {
                    oldUserIdOfUpdateDetailCollectionUpdateDetail.getUpdateDetailCollection().remove(updateDetailCollectionUpdateDetail);
                    oldUserIdOfUpdateDetailCollectionUpdateDetail = em.merge(oldUserIdOfUpdateDetailCollectionUpdateDetail);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsers(users.getUserId()) != null) {
                throw new PreexistingEntityException("Users " + users + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Users users) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users persistentUsers = em.find(Users.class, users.getUserId());
            Role roleIdOld = persistentUsers.getRoleId();
            Role roleIdNew = users.getRoleId();
            Collection<Orders> ordersCollectionOld = persistentUsers.getOrdersCollection();
            Collection<Orders> ordersCollectionNew = users.getOrdersCollection();
            Collection<UpdateDetail> updateDetailCollectionOld = persistentUsers.getUpdateDetailCollection();
            Collection<UpdateDetail> updateDetailCollectionNew = users.getUpdateDetailCollection();
            List<String> illegalOrphanMessages = null;
            for (UpdateDetail updateDetailCollectionOldUpdateDetail : updateDetailCollectionOld) {
                if (!updateDetailCollectionNew.contains(updateDetailCollectionOldUpdateDetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain UpdateDetail " + updateDetailCollectionOldUpdateDetail + " since its userId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (roleIdNew != null) {
                roleIdNew = em.getReference(roleIdNew.getClass(), roleIdNew.getRoleId());
                users.setRoleId(roleIdNew);
            }
            Collection<Orders> attachedOrdersCollectionNew = new ArrayList<Orders>();
            for (Orders ordersCollectionNewOrdersToAttach : ordersCollectionNew) {
                ordersCollectionNewOrdersToAttach = em.getReference(ordersCollectionNewOrdersToAttach.getClass(), ordersCollectionNewOrdersToAttach.getOrderId());
                attachedOrdersCollectionNew.add(ordersCollectionNewOrdersToAttach);
            }
            ordersCollectionNew = attachedOrdersCollectionNew;
            users.setOrdersCollection(ordersCollectionNew);
            Collection<UpdateDetail> attachedUpdateDetailCollectionNew = new ArrayList<UpdateDetail>();
            for (UpdateDetail updateDetailCollectionNewUpdateDetailToAttach : updateDetailCollectionNew) {
                updateDetailCollectionNewUpdateDetailToAttach = em.getReference(updateDetailCollectionNewUpdateDetailToAttach.getClass(), updateDetailCollectionNewUpdateDetailToAttach.getId());
                attachedUpdateDetailCollectionNew.add(updateDetailCollectionNewUpdateDetailToAttach);
            }
            updateDetailCollectionNew = attachedUpdateDetailCollectionNew;
            users.setUpdateDetailCollection(updateDetailCollectionNew);
            users = em.merge(users);
            if (roleIdOld != null && !roleIdOld.equals(roleIdNew)) {
                roleIdOld.getUsersCollection().remove(users);
                roleIdOld = em.merge(roleIdOld);
            }
            if (roleIdNew != null && !roleIdNew.equals(roleIdOld)) {
                roleIdNew.getUsersCollection().add(users);
                roleIdNew = em.merge(roleIdNew);
            }
            for (Orders ordersCollectionOldOrders : ordersCollectionOld) {
                if (!ordersCollectionNew.contains(ordersCollectionOldOrders)) {
                    ordersCollectionOldOrders.setUserId(null);
                    ordersCollectionOldOrders = em.merge(ordersCollectionOldOrders);
                }
            }
            for (Orders ordersCollectionNewOrders : ordersCollectionNew) {
                if (!ordersCollectionOld.contains(ordersCollectionNewOrders)) {
                    Users oldUserIdOfOrdersCollectionNewOrders = ordersCollectionNewOrders.getUserId();
                    ordersCollectionNewOrders.setUserId(users);
                    ordersCollectionNewOrders = em.merge(ordersCollectionNewOrders);
                    if (oldUserIdOfOrdersCollectionNewOrders != null && !oldUserIdOfOrdersCollectionNewOrders.equals(users)) {
                        oldUserIdOfOrdersCollectionNewOrders.getOrdersCollection().remove(ordersCollectionNewOrders);
                        oldUserIdOfOrdersCollectionNewOrders = em.merge(oldUserIdOfOrdersCollectionNewOrders);
                    }
                }
            }
            for (UpdateDetail updateDetailCollectionNewUpdateDetail : updateDetailCollectionNew) {
                if (!updateDetailCollectionOld.contains(updateDetailCollectionNewUpdateDetail)) {
                    Users oldUserIdOfUpdateDetailCollectionNewUpdateDetail = updateDetailCollectionNewUpdateDetail.getUserId();
                    updateDetailCollectionNewUpdateDetail.setUserId(users);
                    updateDetailCollectionNewUpdateDetail = em.merge(updateDetailCollectionNewUpdateDetail);
                    if (oldUserIdOfUpdateDetailCollectionNewUpdateDetail != null && !oldUserIdOfUpdateDetailCollectionNewUpdateDetail.equals(users)) {
                        oldUserIdOfUpdateDetailCollectionNewUpdateDetail.getUpdateDetailCollection().remove(updateDetailCollectionNewUpdateDetail);
                        oldUserIdOfUpdateDetailCollectionNewUpdateDetail = em.merge(oldUserIdOfUpdateDetailCollectionNewUpdateDetail);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = users.getUserId();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getUserId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<UpdateDetail> updateDetailCollectionOrphanCheck = users.getUpdateDetailCollection();
            for (UpdateDetail updateDetailCollectionOrphanCheckUpdateDetail : updateDetailCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Users (" + users + ") cannot be destroyed since the UpdateDetail " + updateDetailCollectionOrphanCheckUpdateDetail + " in its updateDetailCollection field has a non-nullable userId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Role roleId = users.getRoleId();
            if (roleId != null) {
                roleId.getUsersCollection().remove(users);
                roleId = em.merge(roleId);
            }
            Collection<Orders> ordersCollection = users.getOrdersCollection();
            for (Orders ordersCollectionOrders : ordersCollection) {
                ordersCollectionOrders.setUserId(null);
                ordersCollectionOrders = em.merge(ordersCollectionOrders);
            }
            em.remove(users);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }

    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Users findUsers(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsersCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
