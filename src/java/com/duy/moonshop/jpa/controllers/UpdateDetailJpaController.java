/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.duy.moonshop.jpa.controllers;

import com.duy.moonshop.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.duy.moonshop.jpa.entity.Cake;
import com.duy.moonshop.jpa.entity.UpdateDetail;
import com.duy.moonshop.jpa.entity.Users;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author duy
 */
public class UpdateDetailJpaController implements Serializable {

    public UpdateDetailJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(UpdateDetail updateDetail) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cake cakeId = updateDetail.getCakeId();
            if (cakeId != null) {
                cakeId = em.getReference(cakeId.getClass(), cakeId.getCakeId());
                updateDetail.setCakeId(cakeId);
            }
            Users userId = updateDetail.getUserId();
            if (userId != null) {
                userId = em.getReference(userId.getClass(), userId.getUserId());
                updateDetail.setUserId(userId);
            }
            em.persist(updateDetail);
            if (cakeId != null) {
                cakeId.getUpdateDetailCollection().add(updateDetail);
                cakeId = em.merge(cakeId);
            }
            if (userId != null) {
                userId.getUpdateDetailCollection().add(updateDetail);
                userId = em.merge(userId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UpdateDetail updateDetail) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UpdateDetail persistentUpdateDetail = em.find(UpdateDetail.class, updateDetail.getId());
            Cake cakeIdOld = persistentUpdateDetail.getCakeId();
            Cake cakeIdNew = updateDetail.getCakeId();
            Users userIdOld = persistentUpdateDetail.getUserId();
            Users userIdNew = updateDetail.getUserId();
            if (cakeIdNew != null) {
                cakeIdNew = em.getReference(cakeIdNew.getClass(), cakeIdNew.getCakeId());
                updateDetail.setCakeId(cakeIdNew);
            }
            if (userIdNew != null) {
                userIdNew = em.getReference(userIdNew.getClass(), userIdNew.getUserId());
                updateDetail.setUserId(userIdNew);
            }
            updateDetail = em.merge(updateDetail);
            if (cakeIdOld != null && !cakeIdOld.equals(cakeIdNew)) {
                cakeIdOld.getUpdateDetailCollection().remove(updateDetail);
                cakeIdOld = em.merge(cakeIdOld);
            }
            if (cakeIdNew != null && !cakeIdNew.equals(cakeIdOld)) {
                cakeIdNew.getUpdateDetailCollection().add(updateDetail);
                cakeIdNew = em.merge(cakeIdNew);
            }
            if (userIdOld != null && !userIdOld.equals(userIdNew)) {
                userIdOld.getUpdateDetailCollection().remove(updateDetail);
                userIdOld = em.merge(userIdOld);
            }
            if (userIdNew != null && !userIdNew.equals(userIdOld)) {
                userIdNew.getUpdateDetailCollection().add(updateDetail);
                userIdNew = em.merge(userIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = updateDetail.getId();
                if (findUpdateDetail(id) == null) {
                    throw new NonexistentEntityException("The updateDetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            UpdateDetail updateDetail;
            try {
                updateDetail = em.getReference(UpdateDetail.class, id);
                updateDetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The updateDetail with id " + id + " no longer exists.", enfe);
            }
            Cake cakeId = updateDetail.getCakeId();
            if (cakeId != null) {
                cakeId.getUpdateDetailCollection().remove(updateDetail);
                cakeId = em.merge(cakeId);
            }
            Users userId = updateDetail.getUserId();
            if (userId != null) {
                userId.getUpdateDetailCollection().remove(updateDetail);
                userId = em.merge(userId);
            }
            em.remove(updateDetail);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<UpdateDetail> findUpdateDetailEntities() {
        return findUpdateDetailEntities(true, -1, -1);
    }

    public List<UpdateDetail> findUpdateDetailEntities(int maxResults, int firstResult) {
        return findUpdateDetailEntities(false, maxResults, firstResult);
    }

    private List<UpdateDetail> findUpdateDetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(UpdateDetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public UpdateDetail findUpdateDetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UpdateDetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getUpdateDetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<UpdateDetail> rt = cq.from(UpdateDetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
