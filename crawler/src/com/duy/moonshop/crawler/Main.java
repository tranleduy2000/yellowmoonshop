package com.duy.moonshop.crawler;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    private static final String OUTPUT_PATH = "C:\\Users\\duy\\Documents\\NetBeansProjects\\YellowMoonShop\\data";
    private static final String BASE_URL = "https://www.bakingo.com";
    private static final String[] CATEGORY_URLS = {
            "/chocolate-cakes", "/eggless-cakes", "/black-forest-cakes", "/butterscotch-cakes",
            "/strawberry-cakes", "/red-velvet-cakes", "/vanilla-cakes", "/pineapple-cakes",
            "/fruit-cakes", "/blueberry-cakes"};

    public static void main(String[] args) throws IOException, InterruptedException {
        File imageDir = new File(OUTPUT_PATH, "images");
        imageDir.mkdirs();
        File queryDir = new File(OUTPUT_PATH, "sqlscripts");
        queryDir.mkdirs();

        List<String> queries = new ArrayList<>();
        HashMap<String, Boolean> existingMap = new HashMap<>();

        queries.add("DELETE FROM Cake WHERE name LIKE '%%'");
        queries.add("DELETE FROM Category WHERE name LIKE '%%'");
        for (int categoryIndex = 0, categoryUrlsLength = CATEGORY_URLS.length; categoryIndex < categoryUrlsLength; categoryIndex++) {
            String categoryUrl = CATEGORY_URLS[categoryIndex];

            int totalPages;
            {
                System.out.println("BEGIN Fetch " + BASE_URL + categoryUrl);
                Document page = Jsoup.connect(BASE_URL + categoryUrl).get();
                System.out.println("END Fetch " + BASE_URL + categoryUrl);
                Element tag = page.select("#__PRELOADED_STATE__").first();
                JSONObject jsonObject = new JSONObject(tag.childNode(0).toString());
                JSONObject productObject = jsonObject.getJSONObject("product");
                JSONObject productListPager = productObject.getJSONObject("productListPager");
                totalPages = productListPager.getInt("total_pages");
            }

            queries.add(String.format("INSERT INTO Category(categoryId, name) VALUES (%d, '%s')", categoryIndex,
                    WordUtils.capitalizeFully(categoryUrl.replace("/", "").replace("-", " "))));

            for (int pageIndex = 0; pageIndex < totalPages; pageIndex++) {
                System.out.println("BEGIN Fetch " + BASE_URL + categoryUrl + "?page=" + pageIndex);
                Document page = Jsoup.connect(BASE_URL + categoryUrl + "?page=" + pageIndex).get();
                System.out.println("END Fetch " + BASE_URL + categoryUrl + "?page=" + pageIndex);
                Element tag = page.select("#__PRELOADED_STATE__").first();
                JSONObject jsonObject = new JSONObject(tag.childNode(0).toString());
                JSONObject productObject = jsonObject.getJSONObject("product");
                JSONArray productList = productObject.getJSONArray("productList");

                for (int i = 0; i < productList.length(); i++) {
                    JSONObject product = productList.getJSONObject(i);

                    String imageUrl = product.getString("image_url");
                    String imageName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1);
                    if (imageName.contains("?")) {
                        imageName = imageName.substring(0, imageName.indexOf("?"));
                    }

                    download(imageUrl, new File(imageDir, imageName));
                    String title = product.getString("title");
                    double price = product.getDouble("sell_price");
                    String miniDesc = product.getString("mini_desc");
                    if (!existingMap.containsKey(title)) {
                        queries.add(
                                String.format(
                                        "INSERT INTO Cake(name,description,imagePath,price,createDate,expirationDate,categoryId,quantity) " +
                                                "VALUES ('%s','%s', '%s', %f,'%s', '%s', %d, %d)",
                                        escapeSql(title), escapeSql(miniDesc), imageName, price,
                                        new Date(System.currentTimeMillis()).toString(),
                                        new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(300)).toString(), categoryIndex, 10)
                        );
                        existingMap.put(title, true);
                    }
                }
            }


        }
        writeLines(queries, new File(queryDir, "data.sql"));
    }

    private static void download(String imageUrl, File output) {
        Runnable runnable = () -> {
            try {
                System.out.println("begin download = " + imageUrl);
                URLConnection urlConnection = new URL(imageUrl.replace(" ", "%20")).openConnection();
                InputStream inputStream = urlConnection.getInputStream();

                FileOutputStream fileOutputStream = new FileOutputStream(output);
                IOUtils.copy(inputStream, fileOutputStream);
                fileOutputStream.close();
                System.out.println("download success imageUrl = " + imageUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        runnable.run();
    }

    private static String escapeSql(String sql) {
        return sql.replace("'", "''");
    }

    private static void writeLines(List<String> queries, File file) throws IOException {
        StringBuilder builder = new StringBuilder();
        for (String query : queries) {
            builder.append(query).append("\n");
        }
        FileUtils.write(file, builder);
    }

}
